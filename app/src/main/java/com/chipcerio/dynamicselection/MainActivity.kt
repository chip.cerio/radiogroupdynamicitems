package com.chipcerio.dynamicselection

import android.os.Bundle
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.chipcerio.dynamicselection.data.Response
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        // read json file in assets
        val jsonFile = "single.json"
        val jsonContent = assets.open(jsonFile).bufferedReader().use {
            it.readText()
        }
        
        // serialize
        val gson = Gson()
        val response = gson.fromJson(jsonContent, Response::class.java)
    
        // filter active
        val activeList = response.data.package_pricing.single.filter { it.is_active }
        
        // display
        for (item in activeList) {
            val radio = RadioButton(this)
            radio.text = "${item.quantity}: ${item.symbol}${item.price}"
            radio.id = item.package_id
            radioGroupNum.addView(radio)
        }
        
        // selecting questions
        radioGroupNum.setOnCheckedChangeListener { _, checkedId ->
            println(checkedId)
            
            val selected = activeList.find { it.package_id == checkedId }
            
            selected?.let {
                textViewQty.text = "${it.quantity}"
                textViewPrice.text = "${it.symbol} ${it.price}"
            }
        }
        
        // default mid item selection
        val selectedMiddleIndex = medianOf(activeList.size)
        val selectedRadioId = activeList[selectedMiddleIndex].package_id
        radioGroupNum.check(selectedRadioId)
        
    }
    
    private fun medianOf(size: Int): Int {
        return (size / 2).toDouble().roundToInt()
    }
}