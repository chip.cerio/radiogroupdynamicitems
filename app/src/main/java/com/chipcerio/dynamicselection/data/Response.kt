package com.chipcerio.dynamicselection.data

data class Response(
    val success: String = "",
    val message: String = "",
    val http_status: Int = 0,
    val data: Data = Data(),
)

data class Data(
    val package_pricing: PackagePricing = PackagePricing()
)

data class PackagePricing(
    val monthly: List<Pricing> = emptyList(),
    val single: List<Pricing> = emptyList(),
    val vip: Vip = Vip(),
)

data class Pricing(
    val package_id: Int = 0,
    val quantity: Int = 0,
    val price: Double = 0.0,
    val currency: String = "",
    val symbol: String = "",
    val stripe_plan_id: String? = "",
    val is_active: Boolean = false,
)

data class Vip(
    val package_id: Int = 0,
    val price: Double = 0.0,
    val currency: String = "",
    val symbol: String = "",
    val stripe_plan_id: String? = "",
)
